SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema car
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `car` ;

-- -----------------------------------------------------
-- Schema car
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `car` DEFAULT CHARACTER SET utf8 ;
USE `car` ;

-- -----------------------------------------------------
-- Table `car`.`cars`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `car`.`cars` ;

CREATE TABLE IF NOT EXISTS `car`.`cars` (
  `id_cars` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `price` INT NULL,
  `price_total` INT NULL,
  PRIMARY KEY (`id_cars`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `car`.`Accessory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `car`.`Accessory` ;

CREATE TABLE IF NOT EXISTS `car`.`Accessory` (
  `id_accessory` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `price` INT NULL,
  PRIMARY KEY (`id_accessory`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `car`.`car_and_accesory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `car`.`accessory_cars` ;

CREATE TABLE IF NOT EXISTS `car`.`car_and_accesory` (
  `cars_id` INT NOT NULL,
  `Accessory_id` INT NOT NULL,
  PRIMARY KEY (`cars_id`, `Accessory_id`),
  INDEX `fk_cars_has_Accessory_Accessory1_idx` (`Accessory_id` ASC),
  INDEX `fk_cars_has_Accessory_cars_idx` (`cars_id` ASC),
  CONSTRAINT `fk_cars_has_Accessory_cars`
    FOREIGN KEY (`cars_id`)
    REFERENCES `car`.`cars` (`id_cars`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cars_has_Accessory_Accessory1`
    FOREIGN KEY (`Accessory_id`)
    REFERENCES `car`.`Accessory` (`id_accessory`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
