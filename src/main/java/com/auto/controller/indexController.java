package com.auto.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * Created by max12 on 21/5/2017.
 */
@Controller
public class indexController {

    @RequestMapping("/")
    public String index(){
        return "redirect:swagger-ui.html";
    }

}
