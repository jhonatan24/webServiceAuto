package com.auto.controller;

import com.auto.dao.AccesoryDao;
import com.auto.dao.CarsDao;
import com.auto.models.Accessory;
import com.auto.models.Cars;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by max12 on 21/5/2017.
 */
@RestController
public class CarsController {
    @Autowired
    private CarsDao carsDao;
    @Autowired
    private AccesoryDao AccessoryDao;


    @RequestMapping(value = "/api/Cars",method = RequestMethod.POST)
    public Long CreateCars(@RequestParam(value = "name",required = true) String name,
                               @RequestParam(value = "price",required = true) int price,
                               @RequestParam(value = "IdAccesory",required = true) List<Long> accesory){
      try {
          int priceTotal = price;
          List<Accessory> pojo = null;
          pojo = (List<Accessory>) AccessoryDao.findAll(accesory);
          priceTotal = pojo.stream().mapToInt(p -> p.getPrice()).sum() + price;
          return carsDao.save(new Cars(name, price, pojo,priceTotal)).getId();
      }catch (Exception ex){
          return Long.valueOf(0);
      }
    }
    @RequestMapping(value = "/api/Cars",method = RequestMethod.GET)
    public Iterable<Cars> ListCars(){
        List<Cars> data  = null;
        data = (List<Cars>) carsDao.findAll();
        List<Cars> collect = data.stream().map(car -> {
            List<String> name =  null;
             name = car.getAccessory().stream().map(p -> p.getName()).collect(Collectors.toList());
             car.setOptional(name);
            return car;
        }).collect(Collectors.toList());
        return collect;
    }

    @RequestMapping(value = "/api/Cars/{id}}",method = RequestMethod.DELETE)
    public boolean DeleteCars(@PathVariable Long id){
        try {
            carsDao.delete(id);
        }catch (Exception ex){
            return false;
        }

        return true;
    }

    @RequestMapping(value = "/api/Cars/{id}}",method = RequestMethod.GET)
    public Cars OneCars(@PathVariable Long id)
    {
        Cars data  = null;
        data =  carsDao.findOne(id);
        List collect = null;
        collect = data.getAccessory().stream().map(p -> p.getName()).collect(Collectors.toList());
        data.setOptional(collect);
        return data;
    }

    @RequestMapping(value = "/api/Cars/{id}}",method = RequestMethod.PUT)
    public boolean UpdateCars(
            @PathVariable Long id,
            @RequestParam(value = "name",required = false) String name,
            @RequestParam(value = "price",required = false) int price,
            @RequestParam(value = "IdAccesory",required = true) List<Long> accesory){
        try {

            Cars pojo = carsDao.findOne(id);
            pojo.setName(name);
            pojo.setPrice(price);
            List<Accessory> Data = null;
            int priceTotal = price;
            Data = (List<Accessory>) AccessoryDao.findAll(accesory);
            priceTotal = Data.stream().mapToInt(p -> p.getPrice()).sum() + price;
            pojo.setPriceTotal(priceTotal);
            Data = (List<Accessory>) AccessoryDao.findAll(accesory);
            pojo.setAccessory(Data);
            carsDao.save(pojo);
            return true;
        }catch (Exception ex){
            return false;
        }
    }

}
