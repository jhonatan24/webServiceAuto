package com.auto.controller;

import com.auto.dao.AccesoryDao;
import com.auto.models.Accessory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by max12 on 21/5/2017.
 */
@RestController
public class AccesoryController {

    @Autowired
    private AccesoryDao Accesory;

    @RequestMapping(value = "/api/Accesory",method = RequestMethod.POST)
    public Long CreateAccesory(@RequestParam(value = "name",required = true) String name,
                               @RequestParam(value = "price",required = true) int price){
        try {
            return  Accesory.save(new Accessory(name,price)).getIdAccesory();
        }catch (Exception ex){
            return Long.valueOf(0);
        }
    }
    @RequestMapping(value = "/api/Accesory",method = RequestMethod.GET)
    public Iterable<Accessory> ListAccesory(){
       return  Accesory.findAll();
    }

    @RequestMapping(value = "/api/Accesory/{id}}",method = RequestMethod.DELETE)
    public boolean DeleteAccesory(@PathVariable Long id){
        try {
            Accesory.delete(id);
        }catch (Exception ex){
            return false;
        }
        return true;
    }

    @RequestMapping(value = "/api/Accesory/{id}}",method = RequestMethod.GET)
    public Accessory OneAccesory(@PathVariable Long id){
        return Accesory.findOne(id);
    }

    @RequestMapping(value = "/api/Accesory/{id}}",method = RequestMethod.PUT)
    public boolean UpdateAccesory(
            @PathVariable Long id,
            @RequestParam(value = "name",required = false) String name,
            @RequestParam(value = "price",required = false) int price){
       try {
           Accessory pojo = Accesory.findOne(id);
           pojo.setName(name);
           pojo.setPrice(price);
           Accesory.save(pojo);
           return true;
       }catch (Exception ex){
           return false;
       }
    }




}
