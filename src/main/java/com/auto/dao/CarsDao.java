package com.auto.dao;

import com.auto.models.Cars;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by max12 on 21/5/2017.
 */
@Service
@Transactional
public interface CarsDao extends CrudRepository<Cars,Long> {

}
