package com.auto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebServiceAutoApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebServiceAutoApplication.class, args);
	}


}
