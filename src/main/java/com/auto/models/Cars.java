package com.auto.models;


import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.util.ArrayIterator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by max12 on 21/5/2017.
 */
@Entity
public class Cars {
   // implements Serializable
    // private static final long serialVersionUID = 1L;

    @JsonProperty
    String name;
    @JsonProperty
    int price;
    @Id @GeneratedValue @Column(name = "id_cars")  @JsonProperty
    Long id;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "accessory_cars",
            joinColumns=@JoinColumn(name="cars_id_cars", referencedColumnName="id_cars"),
            inverseJoinColumns=@JoinColumn(name="accessory_id_accessory", referencedColumnName="id_accessory"))
    List<Accessory> Accessory;
    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Column(name = "price_total")
    int priceTotal;
    @JsonProperty
    @Transient
   private List optional;



    public Cars( int price, String name) {
        this.price = price;
        this.name = name;
    }

    public Cars(String name, int price, List<Accessory> accessory) {
        this.name = name;
        this.price = price;
        Accessory = accessory;
    }

    public Cars(String name, int price, List<com.auto.models.Accessory> accessory, int priceTotal) {
        this.name = name;
        this.price = price;
        Accessory = accessory;
        this.priceTotal = priceTotal;
    }

    public Cars() {
    }
    @JsonIgnore
    public List<Accessory> getAccessory() {
        return Accessory;
    }

    public void setAccessory(List<Accessory> accessory) {
        Accessory = accessory;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Cars{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", id=" + id +
                ", Accessory=" + Accessory +
                '}';
    }

    public int getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(int priceTotal) {
        this.priceTotal = priceTotal;
    }

    public List getOptional() {
        return optional;
    }

    public void setOptional(List optional) {
        this.optional = optional;
    }
}
