package com.auto.models;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by max12 on 21/5/2017.
 */
@Entity
public class Accessory  {
   // implements Serializable
   // private static final long serialVersionUID = 1L;
    @JsonProperty
    @Id @GeneratedValue
    @Column(name = "id_accessory")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Long idAccesory;
    @JsonProperty
    String name;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty
    int price;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name="accessory_cars",
            joinColumns=@JoinColumn(name="accessory_id_accessory", referencedColumnName="id_accessory"),
            inverseJoinColumns=@JoinColumn(name="cars_id_cars", referencedColumnName="id_cars"))
    @JsonIgnore
    List<Cars> cars;

    public Accessory() {
    }

    public Accessory( String name, int price) {
        this.name = name;
        this.price = price;
    }

    public Accessory( String name, int price, List<com.auto.models.Cars> cars) {
        this.name = name;
        this.price = price;
        cars = cars;
    }

    public Long getIdAccesory() {
        return idAccesory;
    }

    public void setIdAccesory(Long idAccesory) {
        this.idAccesory = idAccesory;
    }

    public List<com.auto.models.Cars> getCars() {
        return cars;
    }

    public void setCars(List<com.auto.models.Cars> cars) {
        cars = cars;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Accessory{" +
                "idAccesory=" + idAccesory +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", Cars=" + cars +
                '}';
    }
}
